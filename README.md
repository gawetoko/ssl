Creating config from json file to binary, run this command

./ssl -cmd="createConfig" -config="./config.json"

Change key variable in config/config.go, 32 length.

Install mysql for database, haproxy for load balancer (If you need).

For Build Release https://bitbucket.org/gawetoko/ssl/downloads/

create this mysql table

CREATE TABLE `accounts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL DEFAULT '',
  `privateKey` longtext,
  `domainPrivateKey` longtext,
  `resource` longtext,
  `apiKey` longtext,
  `development` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `config` (
  `pointingAddress` varchar(100) NOT NULL DEFAULT '',
  `gcloudProject` varchar(100) NOT NULL DEFAULT '',
  `gcloudAuth` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `domains` (
  `domain` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(100) DEFAULT NULL,
  `privateKey` longtext,
  `certs` longtext,
  `resources` longtext,
  `serverid` varchar(100) DEFAULT NULL,
  `autorenew` tinyint(4) NOT NULL DEFAULT '1',
  `lastupdate` bigint(20) NOT NULL,
  `globalPrivateKey` tinyint(4) DEFAULT '1',
  `wildcard` tinyint(4) DEFAULT '0',
  `hash` varchar(200) DEFAULT NULL,
  `expired` bigint(20) DEFAULT '0',
  `lastError` longtext,
  PRIMARY KEY (`domain`),
  UNIQUE KEY `domains` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `validation` (
  `token` varchar(200) NOT NULL DEFAULT '',
  `value` longtext NOT NULL,
  `lastupdate` bigint(20) NOT NULL,
  PRIMARY KEY (`token`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
