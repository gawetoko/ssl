package acme

import (
	"bitbucket.org/gawetoko/ssl/models"
	"encoding/json"
	"errors"
	"fmt"
)

func CreateNewAccounts(account *Account) (e error) {
	acc, e := models.GetAccount(account.Email)
	if e != nil || acc.Id == 0 {
		if acc.Id == 0 {
			return errors.New(fmt.Sprintf("Email %s not found", account.Email))
		}
		return e
	}

	account.Development = acc.Development

	if len(acc.PrivateKey) == 0 {
		privKey, e := generatePrivateKey(EC256)
		if e != nil {
			return e
		}

		account.PrivateKey = privKey

		enc := pemEncode(privKey)
		er := models.UpdateAccounts(account.Email, string(enc), nil, nil)
		if er != nil {
			return er
		}

	} else {

		pe, er := loadPrivateKey([]byte(acc.PrivateKey))
		if er != nil {
			return er
		}

		account.PrivateKey = pe

	}

	if len(acc.DomainPrivateKey) == 0 {
		privKey, e := generatePrivateKey(RSA2048)
		if e != nil {
			return e
		}

		account.DomainPrivateKey = privKey

		enc := pemEncode(privKey)
		er := models.UpdateAccounts(account.Email, nil, string(enc), nil)
		if er != nil {
			return er
		}

	} else {

		pe, er := loadPrivateKey([]byte(acc.DomainPrivateKey))
		if er != nil {
			return er
		}

		account.DomainPrivateKey = pe

	}

	if len(acc.Resource) != 0 {
		var res RegistrationResource
		e = json.Unmarshal([]byte(acc.Resource), &res)
		if e == nil {
			account.RegistrationResource = &res
		}
	}

	return nil
}
