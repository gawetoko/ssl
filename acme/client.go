package acme

import (
	"bitbucket.org/gawetoko/ssl/models"
	"crypto"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var interval *time.Ticker
var intervalRenew *time.Ticker

func init() {

	interval = time.NewTicker(1000 * time.Millisecond)
	go func() {
		for _ = range interval.C {
			domain, err := Validation()
			if err != nil {
				log.Println(domain, err)
				if len(domain) != 0 {
					e := models.SetDomainLastUpdate(domain, err)
					if e != nil {
						log.Println(e)
					}
				}
			}
		}
	}()

	intervalRenew = time.NewTicker(1000 * time.Millisecond)
	go func() {
		for _ = range intervalRenew.C {
			e := RenewExpired()
			if e != nil {
				log.Println(e)
			}
		}
	}()
}

func RenewExpired() (e error) {

	ip, e := getIp()
	if e != nil {
		return e
	}

	d, e := models.GetDomainExpired(ip)
	if e != nil {
		return e
	}

	for _, sk := range d {
		accounts := &Account{
			Email: sk.Email,
		}
		errs := CreateNewAccounts(accounts)
		if errs == nil {
			domains := []string{sk.Domain}
			if sk.Wildcard == true {
				domains = append(domains, "*."+sk.Domain)
			}

			client := &Client{
				Account:          accounts,
				DNS:              sk.Domain,
				Domain:           domains,
				GlobalPrivateKey: sk.GlobalPrivateKey,
				Wildcard:         sk.Wildcard,
			}

			errs_ := client.New()
			if errs_ == nil {
				client.Order()
			}
		}
	}

	// Restarting haproxy

	return e
}

func UpdatevalidationSuccess(or orderResource, ss authorization, wildcard bool, domain string) {
	var ass []authorization
	for _, s := range or.Authorizations {
		if s.Wildcard == wildcard {
			s = ss
		}
		ass = append(ass, s)
	}

	or.Authorizations = ass

	var body orderMessage
	_, e := getJSON(or.URL, &body)
	if e != nil {
		log.Println(e)
	}

	or.orderMessage = body
	ls, _ := json.Marshal(or)

	es := models.SetDomainResources(domain, string(ls))
	if es != nil {
		log.Println(es)
	}
}

func Validation() (d string, e error) {
	ip, e := getIp()
	if e != nil {
		return d, e
	}

	ress, e := models.GetDomainResources(nil, ip)
	if e != nil {
		return d, e
	}

	if len(ress.Resources) == 0 {
		return "", nil
	}

	now := time.Now().Unix()
	kl := now - ress.Lastupdate

	var ltt int64 = 60

	if kl < ltt {
		return d, errors.New(fmt.Sprintf("Domain %s to fast update, will be update in %ds again", ress.Domain, ltt-kl))
	}

	d = ress.Domain
	var or orderResource
	e = json.Unmarshal([]byte(ress.Resources), &or)
	if e != nil {
		return d, e
	}

	accounts := &Account{
		Email: or.Email,
	}

	e = CreateNewAccounts(accounts)
	if e != nil {
		return d, e
	}

	var pe crypto.PrivateKey
	if ress.GlobalPrivateKey == false {
		pse, er := loadPrivateKey([]byte(ress.PrivateKey))
		if er != nil {
			return d, er
		}
		pe = pse
	} else {
		pe = accounts.DomainPrivateKey
	}

	client := &Client{
		Account:          accounts,
		DNS:              ress.Domain,
		Domain:           or.Domains,
		DomainPrivateKey: pe,
	}

	e = client.New()
	if e != nil {
		return d, e
	}

	var allchalangevalid, haveinvalid bool = true, false

	for _, s := range or.Authorizations {

		if s.Wildcard == false && s.Status == "pending" {
			allchalangevalid = false

			for _, ss := range s.Challenges {

				if ss.Type == "http-01" && ss.Status == "pending" {

					e = client.validatehttp(client.Account.Jws, s.Identifier.Value, ss.URL, ss)
					if e != nil {
						return d, e
					} else {

						log.Println("Successfully validation http method", ss.URL)
						var auth authorization
						_, e = getJSON(ss.URL, &auth)
						if e != nil {
							return d, e
						}

						UpdatevalidationSuccess(or, auth, false, ress.Domain)
						time.Sleep(4 * time.Second)

					}

				} else if ss.Type == "http-01" && ss.Status == "invalid" {
					haveinvalid = true
				}
			}
		}

		if s.Wildcard == true && s.Status == "pending" {
			allchalangevalid = false
			for _, ss := range s.Challenges {
				if ss.Type == "dns-01" && ss.Status == "pending" {
					e = client.validatedns(client.Account.Jws, s.Identifier.Value, ss.URL, ss)
					if e != nil {

						return d, e

					} else {

						log.Println("Successfully validation dns method ", ss.URL)

						var auth authorization
						_, e = getJSON(ss.URL, &auth)
						if e != nil {
							return d, e
						}

						UpdatevalidationSuccess(or, auth, true, ress.Domain)
						time.Sleep(4 * time.Second)

					}
				} else if ss.Type == "dns-01" && ss.Status == "invalid" {
					haveinvalid = true
				}

			}

		}
	}

	if haveinvalid == true {
		client.recreateOrder()
	}

	if allchalangevalid == true {
		log.Println(fmt.Sprintf("Creating certificate for domain %s", ress.Domain))
		cer, er := client.NewCertificate()
		if er != nil {
			return d, er
		}

		l, er := GetPEMCertExpiration(cer.Certificate)
		if er != nil {
			return d, er
		}

		err := models.SetCertificates(ress.Domain, string(cer.Certificate), generateHash(cer.Certificate), l.Unix())
		if err != nil {
			return d, err
		}

	}

	return d, e

}

func (this *Client) New() error {
	var err error

	config, er := models.GetConfig()
	if er != nil {
		return er
	}
	this.Config = config

	var url string = Production
	if this.Account.Development == true {
		url = Staging
	}

	var dir Directory
	_, err = getJSON(url, &dir)
	if err != nil {
		return err
	}

	this.Account.Directory = dir

	if this.Account.Directory.NewAccount == "" {
		return errors.New("directory missing new registration URL")
	}

	if this.Account.Directory.NewOrder == "" {
		return errors.New("directory missing new order URL")
	}

	jws := &jws{privKey: this.Account.PrivateKey, getNonceURL: dir.NewNonce}
	this.Account.Jws = jws

	if this.Account.RegistrationResource != nil {
		this.Account.Jws.kid = this.Account.RegistrationResource.URI
		return err
	}

	accMsg := accountMessage{}
	accMsg.Contact = []string{"mailto:" + this.Account.Email}
	accMsg.TermsOfServiceAgreed = true

	var serverReg accountMessage
	hdr, err := postJSON(this.Account.Jws, this.Account.Directory.NewAccount, accMsg, &serverReg)
	if err != nil {
		remoteErr, ok := err.(RemoteError)
		if ok && remoteErr.StatusCode == 409 {
		} else {
			return err
		}
	}

	this.Account.RegistrationResource = &RegistrationResource{
		URI:  hdr.Get("Location"),
		Body: serverReg,
	}

	this.Account.Jws.kid = this.Account.RegistrationResource.URI

	kl, _ := json.Marshal(this.Account.RegistrationResource)

	err = models.UpdateAccounts(this.Account.Email, nil, nil, string(kl))
	if err != nil {
		return err
	}

	return err
}

func (this *Client) Order() error {

	ress, err := models.GetDomainResources(this.DNS, nil)
	if err != nil {
		return err
	}

	if len(ress.Resources) != 0 {
		return errors.New("Domain already in validation progress")
	}

	var identifiers []identifier
	for _, domain := range this.Domain {
		identifiers = append(identifiers, identifier{Type: "dns", Value: domain})
	}
	order := orderMessage{
		Identifiers: identifiers,
	}

	var response orderMessage
	hdr, err := postJSON(this.Account.Jws, this.Account.Directory.NewOrder, order, &response)
	if err != nil {
		return err
	}

	var auths []authorization
	this.OrderResource = orderResource{
		Email:          this.Account.Email,
		URL:            hdr.Get("Location"),
		Domains:        this.Domain,
		orderMessage:   response,
		Authorizations: auths,
	}

	var haveerror bool = false
	for _, s := range this.OrderResource.orderMessage.Authorizations {
		var auth authorization
		_, err = getJSON(s, &auth)
		if err != nil {
			haveerror = true
			return err
		}

		this.OrderResource.Authorizations = append(this.OrderResource.Authorizations, auth)
	}

	if haveerror == true {
		return errors.New("Failed to get authorizations resources")
	}

	ip, ep := getIp()
	if ep != nil {
		return ep
	}

	// Generate private key
	var privateKey_ interface{} = nil

	if this.GlobalPrivateKey == false {
		privKey, ess := generatePrivateKey(RSA2048)
		if ess != nil {
			return ess
		}
		privateKey_ = string(pemEncode(privKey))
	}

	lk, _ := json.Marshal(this.OrderResource)
	e := models.InsertDomain(this.DNS, this.Account.Email, string(lk), ip, privateKey_, this.GlobalPrivateKey, this.Wildcard)
	if e != nil {
		return e
	}

	return nil
}

func (this *Client) createOrderForIdentifiers(domains []string) (orderResource, error) {

	var identifiers []identifier
	for _, domain := range domains {
		identifiers = append(identifiers, identifier{Type: "dns", Value: domain})
	}

	order := orderMessage{
		Identifiers: identifiers,
	}

	var response orderMessage
	hdr, err := postJSON(this.Account.Jws, this.Account.Directory.NewOrder, order, &response)
	if err != nil {
		return orderResource{}, err
	}

	orderRes := orderResource{
		URL:          hdr.Get("Location"),
		Domains:      domains,
		orderMessage: response,
	}
	return orderRes, nil
}

func (this *Client) requestCertificateForOrder(order orderResource) (*CertificateResource, error) {
	var err error
	commonName := this.DNS
	var san []string
	for _, auth := range order.Identifiers {
		san = append(san, auth.Value)
	}

	csr, err := generateCsr(this.DomainPrivateKey, commonName, san, false)
	if err != nil {
		return nil, err
	}

	return this.requestCertificateForCsr(order, csr, pemEncode(this.DomainPrivateKey))
}

func (this *Client) requestCertificateForCsr(order orderResource, csr []byte, privateKeyPem []byte) (*CertificateResource, error) {

	csrString := base64.RawURLEncoding.EncodeToString(csr)
	var retOrder orderMessage
	_, error := postJSON(this.Account.Jws, order.Finalize, csrMessage{Csr: csrString}, &retOrder)
	if error != nil {
		return nil, error
	}

	if retOrder.Status == "invalid" {
		return nil, error
	}

	certRes := CertificateResource{
		Domain:     this.DNS,
		CertURL:    retOrder.Certificate,
		PrivateKey: privateKeyPem,
	}

	if retOrder.Status == "valid" {
		// if the certificate is available right away, short cut!
		ok, err := this.checkCertResponse(retOrder, &certRes)
		if err != nil {
			return nil, err
		}

		if ok {
			return &certRes, nil
		}
	}

	maxChecks := 1000
	for i := 0; i < maxChecks; i++ {
		_, err := getJSON(order.URL, &retOrder)
		if err != nil {
			return nil, err
		}
		done, err := this.checkCertResponse(retOrder, &certRes)
		if err != nil {
			return nil, err
		}
		if done {
			break
		}
		if i == maxChecks-1 {
			return nil, errors.New(fmt.Sprintf("polled for certificate %d times; giving up", i))
		}
	}

	return &certRes, nil

}

func (this *Client) NewCertificate() (*CertificateResource, error) {
	var e error

	if len(this.Domain) == 0 {
		return nil, errors.New("No domains to obtain a certificate for")
	}

	order, e := this.createOrderForIdentifiers(this.Domain)
	if e != nil {
		return nil, e
	}

	cer, e := this.requestCertificateForOrder(order)
	if e != nil {
		return nil, e
	}

	return cer, e

}

func (this *Client) checkCertResponse(order orderMessage, certRes *CertificateResource) (bool, error) {

	switch order.Status {
	case "valid":
		resp, err := httpGet(order.Certificate)
		if err != nil {
			return false, err
		}

		cert, err := ioutil.ReadAll(limitReader(resp.Body, maxBodySize))
		if err != nil {
			return false, err
		}

		// The issuer certificate link is always supplied via an "up" link
		// in the response headers of a new certificate.
		links := parseLinks(resp.Header["Link"])
		if link, ok := links["up"]; ok {
			issuerCert, err := this.getIssuerCertificate(link)

			if err != nil {
				// If we fail to acquire the issuer cert, return the issued certificate - do not fail.
				log.Printf("[WARNING][%s] acme: Could not bundle issuer certificate: %v", certRes.Domain, err)
			} else {
				issuerCert = pemEncode(derCertificateBytes(issuerCert))

				// If bundle is true, we want to return a certificate bundle.
				// To do this, we append the issuer cert to the issued cert.
				cert = append(cert, issuerCert...)

				certRes.IssuerCertificate = issuerCert
			}
		}

		/*
			block, er := pemDecode(cert)
			if er != nil {
				log.Println(block)
			}
		*/

		certRes.Certificate = cert
		certRes.CertURL = order.Certificate
		certRes.CertStableURL = order.Certificate
		log.Printf("[INFO][%s] Server responded with a certificate.", certRes.Domain)
		return true, nil

	case "processing":
		return false, nil
	case "invalid":
		return false, errors.New("Order has invalid state: invalid")
	}

	return false, nil
}

func (this *Client) getIssuerCertificate(url string) ([]byte, error) {
	log.Printf("[INFO] acme: Requesting issuer cert from %s", url)
	resp, err := httpGet(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	issuerBytes, err := ioutil.ReadAll(limitReader(resp.Body, maxBodySize))
	if err != nil {
		return nil, err
	}

	_, err = x509.ParseCertificate(issuerBytes)
	if err != nil {
		return nil, err
	}

	return issuerBytes, err
}

func checkhostup(url string) (err error) {
	return retry(repeatLimit, retryTime*time.Second, func() error {
		var checkres CheckResponse
		_, err = getJSON(url, &checkres)
		if err != nil {
			log.Println("failed to connect", url)
			return err
		} else {
			return nil
		}

		return errors.New(fmt.Sprintf("failed to connect", url))
	})
}

func checkdnsup(domain, keyAuth string) (err error) {

	return retry(repeatLimit, retryTime*time.Second, func() error {
		fqdn, value, _ := DNS01Record(domain, keyAuth)

		txt, e := GetTXTrecord(fqdn)
		if e != nil {
			return e
		}

		var found bool = false
		for _, a := range txt {
			if a == value {
				found = true
			}
		}

		if found == false {
			log.Println(fmt.Sprintf("No %s txt record found for domain %s", fqdn, domain))
			return errors.New(fmt.Sprintf("No %s txt record found for domain %s", fqdn, domain))
		}

		return nil
	})

}

func (this *Client) recreateOrder() {
	//models.RemoveDomain(this.DNS)

	models.ResetDomain(this.DNS)
	log.Println("recreateOrder for domain ", this.DNS)
	time.Sleep(4 * time.Second)
	this.Order()

	return

}

func (this *Client) validatedns(j *jws, domain, uri string, c challenge) error {
	var chlng challenge
	var e error
	_, e = NewDNSProviderCredentials(this.Config.GcloudProject)
	if e != nil {
		return e
	}

	cc, e := NewDNSProviderServiceAccount([]byte(this.Config.GcloudAuth))
	if e != nil {
		return e
	}

	keyAuth, err := getKeyAuthorization(c.Token, j.privKey)
	if err != nil {
		return err
	}

	e = cc.Present(domain, c.Token, keyAuth)
	if e != nil {
		return e
	}

	e = checkdnsup(domain, keyAuth)
	if e != nil {
		return e
	}

	log.Println("Good, text records found ... submit to let's encrypt", uri)

	hdr, err := postJSON(j, uri, c, &chlng)
	if err != nil {
		this.recreateOrder()
		return err
	}

	// After the path is sent, the ACME server will access our server.
	// Repeatedly check the server for an updated status on our request.
	for {
		switch chlng.Status {
		case "valid":
			log.Printf("[INFO][%s] The server validated our request", domain)
			ess := cc.CleanUp(domain, c.Token, keyAuth)
			if ess != nil {
				log.Println(ess)
			}
			return nil
		case "pending":
		case "processing":
		case "invalid":
			this.recreateOrder()
			return handleChallengeError(chlng)
		default:
			return errors.New("the server returned an unexpected state")
		}

		ra, err := strconv.Atoi(hdr.Get("Retry-After"))
		if err != nil {
			// The ACME server MUST return a Retry-After.
			// If it doesn't, we'll just poll hard.
			ra = 5
		}
		time.Sleep(time.Duration(ra) * time.Second)

		hdr, err = getJSON(uri, &chlng)
		if err != nil {
			return err
		}
	}

	return nil
}

func (this *Client) validatehttp(j *jws, domain, uri string, c challenge) error {
	var chlng challenge

	keyAuth, err := getKeyAuthorization(c.Token, j.privKey)
	if err != nil {
		return err
	}

	ers := models.InsertToken(c.Token, keyAuth)
	if ers != nil {
		return ers
	}

	aip, erss := GetArecord(this.DNS)
	if erss != nil {
		return erss
	}

	if aip != this.Config.PointingAddress && len(aip) != 0 && len(this.Config.PointingAddress) != 0 {
		return errors.New(fmt.Sprintf("Pointing address must be same with a record, current a record is %s ", aip))
	}

	checkuri := fmt.Sprintf("http://%s/.well-known/acme-challenge/check", domain)
	er := checkhostup(checkuri)
	if er != nil {
		return er
	}

	log.Println("Good, server respon up ... submit to let's encrypt", uri)

	hdr, err := postJSON(j, uri, c, &chlng)
	if err != nil {
		this.recreateOrder()
		return err
	}

	// After the path is sent, the ACME server will access our server.
	// Repeatedly check the server for an updated status on our request.
	for {
		switch chlng.Status {
		case "valid":
			log.Printf("[INFO][%s] The server validated our request", domain)
			ess := models.RemoveToken(c.Token)
			if ess != nil {
				log.Println(ess)
			}
			return nil
		case "pending":
		case "processing":
		case "invalid":
			this.recreateOrder()
			return handleChallengeError(chlng)
		default:
			return errors.New("the server returned an unexpected state")
		}

		ra, err := strconv.Atoi(hdr.Get("Retry-After"))
		if err != nil {
			// The ACME server MUST return a Retry-After.
			// If it doesn't, we'll just poll hard.
			ra = 5
		}
		time.Sleep(time.Duration(ra) * time.Second)

		hdr, err = getJSON(uri, &chlng)
		if err != nil {
			return err
		}
	}
}

func retry(attempts int, sleep time.Duration, f func() error) error {
	if err := f(); err != nil {
		if s, ok := err.(stop); ok {
			// Return the original error for later checking
			return s.error
		}

		if attempts--; attempts > 0 {
			// Add some randomness to prevent creating a Thundering Herd
			//jitter := time.Duration(rand.Int63n(int64(sleep)))
			//sleep = sleep + jitter/2
			//log.Println(sleep)
			time.Sleep(sleep)
			return retry(attempts, sleep, f)
		}
		return err
	}

	return nil
}

type stop struct {
	error
}

func getIp() (ip string, err error) {

	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return ip, err
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				ip = ipnet.IP.String()
			}
		}
	}

	return ip, err

}

func parseLinks(links []string) map[string]string {
	aBrkt := regexp.MustCompile("[<>]")
	slver := regexp.MustCompile("(.+) *= *\"(.+)\"")
	linkMap := make(map[string]string)

	for _, link := range links {

		link = aBrkt.ReplaceAllString(link, "")
		parts := strings.Split(link, ";")

		matches := slver.FindStringSubmatch(parts[1])
		if len(matches) > 0 {
			linkMap[matches[2]] = parts[0]
		}
	}

	return linkMap
}
