package acme

import (
	"fmt"
	"github.com/miekg/dns"
	//"log"
	"crypto/sha256"
	"encoding/base64"
	"time"
)

var defaultNameservers = []string{
	"8.8.8.8:53",
	"8.8.4.4:53",
}

var fqdnToZone = map[string]string{}
var DNSTimeout = 10 * time.Second

func GetArecord(fqdn string) (ip string, er error) {
	r, er := dnsQuery(ToFqdn(fqdn), dns.TypeA, defaultNameservers, true)
	if er != nil {
		return ip, er
	}

	for _, rr := range r.Answer {
		if a, ok := rr.(*dns.A); ok {
			if a.Hdr.Name == ToFqdn(fqdn) {
				ip = fmt.Sprintf("%s", a.A)
			}

		}
	}

	return ip, nil
}

func GetTXTrecord(fqdn string) (txt []string, er error) {
	r, er := dnsQuery(ToFqdn(fqdn), dns.TypeTXT, defaultNameservers, true)
	if er != nil {
		return txt, er
	}

	for _, rr := range r.Answer {
		if a, ok := rr.(*dns.TXT); ok {
			if a.Hdr.Name == ToFqdn(fqdn) {
				txt = a.Txt
			}

		}
	}

	return txt, nil
}

func DNS01Record(domain, keyAuth string) (fqdn string, value string, ttl int) {
	keyAuthShaBytes := sha256.Sum256([]byte(keyAuth))
	// base64URL encoding without padding
	value = base64.RawURLEncoding.EncodeToString(keyAuthShaBytes[:sha256.Size])
	ttl = 10
	fqdn = fmt.Sprintf("_acme-challenge.%s.", domain)
	return
}

func ToFqdn(name string) string {
	n := len(name)
	if n == 0 || name[n-1] == '.' {
		return name
	}
	return name + "."
}

// UnFqdn converts the fqdn into a name removing the trailing dot.
func UnFqdn(name string) string {
	n := len(name)
	if n != 0 && name[n-1] == '.' {
		return name[:n-1]
	}
	return name
}

func ClearFqdnCache() {
	fqdnToZone = map[string]string{}
}

func dnsQuery(fqdn string, rtype uint16, nameservers []string, recursive bool) (in *dns.Msg, err error) {
	m := new(dns.Msg)
	m.SetQuestion(fqdn, rtype)
	m.SetEdns0(4096, false)

	if !recursive {
		m.RecursionDesired = false
	}

	// Will retry the request based on the number of servers (n+1)
	for i := 1; i <= len(nameservers)+1; i++ {
		ns := nameservers[i%len(nameservers)]
		udp := &dns.Client{Net: "udp", Timeout: DNSTimeout}
		in, _, err = udp.Exchange(m, ns)

		if err == dns.ErrTruncated {
			tcp := &dns.Client{Net: "tcp", Timeout: DNSTimeout}
			// If the TCP request succeeds, the err will reset to nil
			in, _, err = tcp.Exchange(m, ns)
		}

		if err == nil {
			break
		}
	}
	return
}

func FindZoneByFqdn(fqdn string, nameservers []string) (string, error) {
	// Do we have it cached?
	if zone, ok := fqdnToZone[fqdn]; ok {
		return zone, nil
	}

	labelIndexes := dns.Split(fqdn)
	for _, index := range labelIndexes {
		domain := fqdn[index:]

		in, err := dnsQuery(domain, dns.TypeSOA, nameservers, true)
		if err != nil {
			return "", err
		}

		// Any response code other than NOERROR and NXDOMAIN is treated as error
		if in.Rcode != dns.RcodeNameError && in.Rcode != dns.RcodeSuccess {
			return "", fmt.Errorf("Unexpected response code '%s' for %s",
				dns.RcodeToString[in.Rcode], domain)
		}

		// Check if we got a SOA RR in the answer section
		if in.Rcode == dns.RcodeSuccess {

			// CNAME records cannot/should not exist at the root of a zone.
			// So we skip a domain when a CNAME is found.
			if dnsMsgContainsCNAME(in) {
				continue
			}

			for _, ans := range in.Answer {
				if soa, ok := ans.(*dns.SOA); ok {
					zone := soa.Hdr.Name
					fqdnToZone[fqdn] = zone
					return zone, nil
				}
			}
		}
	}

	return "", fmt.Errorf("Could not find the start of authority")
}

// dnsMsgContainsCNAME checks for a CNAME answer in msg
func dnsMsgContainsCNAME(msg *dns.Msg) bool {
	for _, ans := range msg.Answer {
		if _, ok := ans.(*dns.CNAME); ok {
			return true
		}
	}
	return false
}
