package acme

import (
	"bitbucket.org/gawetoko/ssl/models"
	"crypto"
	"encoding/json"
	"time"
)

type Challenge string

const (
	Production          = "https://acme-v02.api.letsencrypt.org/directory"
	Staging             = "https://acme-staging-v02.api.letsencrypt.org/directory"
	HTTP01              = Challenge("http-01")
	DNS01               = Challenge("dns-01")
	maxBodySize         = 1024 * 1024
	overallRequestLimit = 18
	repeatLimit         = 10
	retryTime           = 4
)

type Client struct {
	Account          *Account
	DNS              string
	Domain           []string
	OrderResource    orderResource
	DomainPrivateKey crypto.PrivateKey
	GlobalPrivateKey bool
	Wildcard         bool
	Config           models.Config
}

type Directory struct {
	KeyChange  string `json:"keyChange"`
	NewAccount string `json:"newAccount"`
	NewNonce   string `json:"newNonce"`
	NewOrder   string `json:"newOrder"`
	RevokeCert string `json:"revokeCert"`
	Meta       struct {
		CaaIdentities  []string `json:"caaIdentities"`
		TermsOfService string   `json:"termsOfService"`
		Website        string   `json:"website"`
	} `json:"meta"`
}

type Account struct {
	Email                string
	PrivateKey           crypto.PrivateKey
	DomainPrivateKey     crypto.PrivateKey
	Directory            Directory
	Jws                  *jws
	RegistrationResource *RegistrationResource
	Development          bool
}

type challenge struct {
	URL              string      `json:"url"`
	Type             string      `json:"type"`
	Status           string      `json:"status"`
	Token            string      `json:"token"`
	Validated        time.Time   `json:"validated"`
	KeyAuthorization string      `json:"keyAuthorization"`
	Error            RemoteError `json:"error"`
}

type accountMessage struct {
	Status                 string          `json:"status,omitempty"`
	Contact                []string        `json:"contact,omitempty"`
	TermsOfServiceAgreed   bool            `json:"termsOfServiceAgreed,omitempty"`
	Orders                 string          `json:"orders,omitempty"`
	OnlyReturnExisting     bool            `json:"onlyReturnExisting,omitempty"`
	ExternalAccountBinding json.RawMessage `json:"externalAccountBinding,omitempty"`
}

type RegistrationResource struct {
	Body accountMessage `json:"body,omitempty"`
	URI  string         `json:"uri,omitempty"`
}

type identifier struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

type orderMessage struct {
	Status         string       `json:"status,omitempty"`
	Expires        string       `json:"expires,omitempty"`
	Identifiers    []identifier `json:"identifiers"`
	NotBefore      string       `json:"notBefore,omitempty"`
	NotAfter       string       `json:"notAfter,omitempty"`
	Authorizations []string     `json:"authorizations,omitempty"`
	Finalize       string       `json:"finalize,omitempty"`
	Certificate    string       `json:"certificate,omitempty"`
}

type orderResource struct {
	URL            string   `json:"url,omitempty"`
	Domains        []string `json:"domains,omitempty"`
	Email          string   `json:"email,omitempty"`
	orderMessage   `json:"body,omitempty"`
	Authorizations []authorization `json:"authorization,omitempty"`
}

type authorization struct {
	Status     string      `json:"status"`
	Expires    time.Time   `json:"expires"`
	Identifier identifier  `json:"identifier"`
	Challenges []challenge `json:"challenges"`
	Wildcard   bool        `json:"wildcard"`
}

type CheckResponse struct {
	Code int `json:"code"`
}

type csrMessage struct {
	Csr string `json:"csr"`
}

type CertificateResource struct {
	Domain            string `json:"domain"`
	CertURL           string `json:"certUrl"`
	CertStableURL     string `json:"certStableUrl"`
	AccountRef        string `json:"accountRef,omitempty"`
	PrivateKey        []byte `json:"-"`
	Certificate       []byte `json:"-"`
	IssuerCertificate []byte `json:"-"`
	CSR               []byte `json:"-"`
}
