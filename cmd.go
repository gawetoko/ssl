package main

import (
	"bytes"
	"os/exec"
	"strings"
)

func cmdexec(cmds string) (o []byte, e error) {

	parts := strings.Fields(cmds)
	head := parts[0]
	parts = parts[1:len(parts)]

	cmd := exec.Command(head, parts...)
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	err := cmd.Run()

	bt := out.Bytes()
	if len(bt) == 0 {
		return o, err
	}

	return bt, nil
}
