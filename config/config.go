package config

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/json"
	"errors"

	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
)

var config Config

const (
	CONFIGLOC = "/etc/gawetoko/config/ssl.bin"
	CONFIG    = "./config.json"
)

type Server struct {
	Address string `json:"address"`
	Port    int    `json:"port"`
}

type SSL struct {
	Path     string `json:"path"`
	Interval int64  `json:"interval"`
}

type Database struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Username string `json:"username"`
	Password string `json:"password"`
	Database string `json:"database"`
}

type Config struct {
	Server   Server   `json:"server"`
	Database Database `json:"database"`
	SSL      SSL      `json:"ssl"`
}

var key = []byte("Yze+M8P2DD$v]Yj-wwte`j<acFH@~;eF")

func init() {

	var configloc string = CONFIG

	var dat, res []byte
	var err error

	dat, err = ioutil.ReadFile(configloc)
	if err != nil {
		log.Println(err)
		configloc = CONFIGLOC
		dat, err = ioutil.ReadFile(CONFIGLOC)
		if err != nil {
			log.Println(err)
			return
		}
	}

	log.Println("USING CONFIG FILE =>", configloc)

	switch filepath.Ext(configloc) {
	case ".json":
		res = dat
		err = createEncrypt("./ssl.bin", dat)
		if err != nil {
			log.Println(err)
		}
	case ".bin":
		res, err = decrypt(dat, key)
		if err != nil {
			log.Println(err)
			return
		}
	}

	err = json.Unmarshal(res, &config)
	if err != nil {
		log.Println(err)
		return
	}

}

func Get() Config {
	return config
}

func CreateConfigFile(loc string) (e error) {
	dat, e := ioutil.ReadFile(loc)
	if e != nil {
		return e
	}
	return createEncrypt(CONFIGLOC, dat)
}

func createEncrypt(out string, dat []byte) (e error) {

	ciphertext, e := encrypt(dat, key)
	if e != nil {
		return e
	}

	e = os.MkdirAll(path.Dir(out), 0700)
	if e != nil {
		return e
	}

	e = ioutil.WriteFile(out, ciphertext, 0700)
	if e != nil {
		return e
	}

	return nil

}

func encrypt(plaintext []byte, key []byte) ([]byte, error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

func decrypt(ciphertext []byte, key []byte) ([]byte, error) {

	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return nil, errors.New("ciphertext too short")
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	return gcm.Open(nil, nonce, ciphertext, nil)
}
