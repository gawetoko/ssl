package controllers

import (
	"bitbucket.org/gawetoko/ssl/models"
	"bitbucket.org/gawetoko/ssl/user_agent"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

type Basecontroller struct {
	RequestId      string
	ResponseWriter http.ResponseWriter
	Start          time.Time
	Email          string
	Request        *http.Request
	UserAgent      *user_agent.UserAgent
	Response       Response `json:"response" xml:"response"`
}

type Errorresponse struct {
	Message interface{} `json:"message" xml:"message"`
}

type Response struct {
	Code         int         `json:"code" xml:"code"`
	Status       string      `json:"status" xml:"status"`
	Error        interface{} `json:"error" xml:"error"`
	RequestId    string      `json:"requestId" xml:"requestId"`
	ExcutionTime string      `json:"excutionTime" xml:"excutionTime"`
	Data         interface{} `json:"data" xml:"data"`
}

func Create(w http.ResponseWriter, r *http.Request) Basecontroller {

	uuid, _ := generateUUID()
	t := Basecontroller{
		RequestId:      uuid,
		UserAgent:      user_agent.New(r.Header.Get("User-Agent"), r.Header),
		ResponseWriter: w,
		Request:        r,
		Start:          time.Now(),
		Response: Response{
			RequestId: uuid,
		},
	}

	return t
}

func (this *Basecontroller) Auth() bool {
	a := strings.Split(this.Request.Header.Get("Authorization"), "::")
	if len(a) != 2 {
		this.Response.Code = 500
		this.Response.Error = Errorresponse{Message: fmt.Sprintf("Authorization format is failed")}
		this.Output()
		return false
	}

	this.Email = a[0]

	acc, e := models.GetAccount(a[0])
	if e != nil {
		this.Response.Code = 500
		this.Response.Error = Errorresponse{Message: fmt.Sprintf("%s", e)}
		this.Output()
		return false
	}

	if acc.Id == 0 {
		this.Response.Code = 401
		this.Response.Error = Errorresponse{Message: fmt.Sprintf("Email %s not found", a[0])}
		this.Output()
		return false
	}

	if len(acc.ApiKey) == 0 || acc.ApiKey != a[1] {
		this.Response.Code = 401
		this.Response.Error = Errorresponse{Message: fmt.Sprintf("Invalid api key")}
		this.Output()
		return false
	}

	return true
}

func (this *Basecontroller) Output() {
	this.Response.ExcutionTime = fmt.Sprintf("%s", time.Since(this.Start))
	defaultHeader(this.ResponseWriter)

	var out []byte

	switch this.Response.Code {
	case 500:
		this.Response.Status = "Server error"
		break
	case 401:
		this.Response.Status = "Unauthorized"
		break
	case 400:
		this.Response.Status = "Bad Request"
		break
	case 403:
		this.Response.Status = "Forbidden"
		break
	case 404:
		this.Response.Status = "Not Found"
		break
	default:
		this.Response.Status = "Success"
		this.Response.Code = 200
	}

	this.ResponseWriter.Header().Set("Content-Type", "application/json")
	out, _ = json.Marshal(this.Response)

	this.ResponseWriter.WriteHeader(this.Response.Code)
	this.ResponseWriter.Write(out)

	return
}

func defaultHeader(w http.ResponseWriter) {

	w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Authorization, Content-Type")
	w.Header().Set("Access-Control-Allow-Method", "GET, POST, OPTION, DELETE, PUT")
	w.Header().Set("Connection", "close")

}

func (this *Basecontroller) GetBodyData(p interface{}) (err error) {
	data, err := ioutil.ReadAll(this.Request.Body)
	defer this.Request.Body.Close()

	if err != nil {
		return err
	}

	es := json.Unmarshal(data, p)
	if es != nil {
		return es
	}

	return nil
}

func generateUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}
