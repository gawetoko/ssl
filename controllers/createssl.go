package controllers

import (
	"bitbucket.org/gawetoko/ssl/acme"
	"fmt"
	"net/http"
)

type CreateSSL struct {
	Basecontroller
}

type CreateSSLRequest struct {
	Email            string `json:"email"`
	Domain           string `json:"domain"`
	Wildcard         bool   `json:"wildcard"`
	GlobalPrivateKey bool   `json:"globalPrivateKey"`
}

func (this *CreateSSL) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	this = &CreateSSL{Create(w, r)}

	if this.Auth() == false {
		return
	}

	var data CreateSSLRequest
	err := this.GetBodyData(&data)
	if err != nil {
		this.Response.Code = 500
		this.Response.Error = Errorresponse{Message: fmt.Sprintf("%s", err)}
		this.Output()
		return
	}

	if this.Email != data.Email {
		this.Response.Code = 401
		this.Response.Error = Errorresponse{Message: fmt.Sprintf("Email must be same with authorization")}
		this.Output()
		return
	}

	accounts := &acme.Account{
		Email: data.Email,
	}
	errs := acme.CreateNewAccounts(accounts)
	if errs != nil {
		this.Response.Code = 400
		this.Response.Error = Errorresponse{Message: fmt.Sprintf("%s", errs)}
		this.Output()
		return
	}

	domains := []string{data.Domain}
	if data.Wildcard == true {
		domains = append(domains, "*."+data.Domain)
	}

	client := &acme.Client{
		Account:          accounts,
		DNS:              data.Domain,
		Domain:           domains,
		GlobalPrivateKey: data.GlobalPrivateKey,
		Wildcard:         data.Wildcard,
	}

	errs_ := client.New()
	if errs_ != nil {
		this.Response.Code = 500
		this.Response.Error = Errorresponse{Message: fmt.Sprintf("%s", errs_)}
		this.Output()
		return

	}

	ers := client.Order()
	if ers != nil {
		this.Response.Code = 500
		this.Response.Error = Errorresponse{Message: fmt.Sprintf("%s", ers)}
		this.Output()
		return

	}

	this.Output()
	return
}
