package controllers

import (
	"bitbucket.org/gawetoko/ssl/models"
	"github.com/gorilla/mux"
	"net/http"
)

type Validation struct {
	Basecontroller
}

func (this *Validation) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	this = &Validation{Create(w, r)}

	vars := mux.Vars(r)

	token, er := models.GetValidation(vars["key"])

	if er != nil {
		this.Response.Code = 500
		this.Output()
		return
	}

	this.ResponseWriter.Write([]byte(token.Value))
	return
}
