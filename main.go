package main

import (
	"bitbucket.org/gawetoko/ssl/config"
	"bitbucket.org/gawetoko/ssl/models"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"path"
	"strings"
	"time"
)

const (
	email = "domain@gawetoko.com"
)

var interval *time.Ticker

/* Auto create pem and copy to ssl directory */

func init() {

	conf := config.Get()
	c := conf.SSL

	interval = time.NewTicker(time.Duration(c.Interval) * time.Second)
	go func() {
		for _ = range interval.C {
			createssl()
		}
	}()
}

func createssl() {

	conf := config.Get()
	c := conf.SSL

	do, e := models.GetallDomain()
	if e != nil {
		log.Println(e)
		return
	}

	for _, m := range do {
		pp := path.Join(c.Path, m.Domain+".pem")
		cc := fmt.Sprintf("%s%s", m.PrivateKey, m.Certs)
		err := ioutil.WriteFile(pp, []byte(cc), 0666)
		if err != nil {
			log.Println(err)
		}
	}

	// Restarting haproxy

	o, es := cmdexec("sudo /etc/init.d/haproxy reload")
	if es != nil {
		log.Println(es)
		return
	}
	sc := fmt.Sprintf("%q", string(o))

	if strings.Contains(sc, "failed") == true {
		log.Println(sc)
	}

}

func main() {

	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var cmd, fileconfig string
	flag.StringVar(&cmd, "cmd", "null", "Run optional command")
	flag.StringVar(&fileconfig, "config", "null", "File config location")
	flag.Parse()

	switch cmd {
	case "createConfig":
		e := config.CreateConfigFile(fileconfig)
		if e != nil {
			log.Println(e)
		}
		return
	}

	c := make(chan int)
	server := &Server{}

	go func() {
		server.Run()
		c <- 1
	}()

	<-c

}
