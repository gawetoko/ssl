package models

import (
	"bitbucket.org/gawetoko/ssl/config"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
	"time"
)

var _ = fmt.Sprintf

var db *sql.DB

func init() {
	open()
}

func open() (dbs *sql.DB, err error) {
	conf := config.Get()
	c := conf.Database
	dbp := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", c.Username, c.Password, c.Host, c.Port, c.Database)
	db, err = sql.Open("mysql", dbp)
	if err != nil {
		log.Println(err)
	}
	return db, err
}

func GetConfig() (s Config, e error) {

	query := fmt.Sprintf("SELECT pointingAddress, gcloudProject, gcloudAuth FROM config LIMIT 1")
	rows, e := db.Query(query)
	if e != nil {
		return s, e
	}
	for rows.Next() {
		rows.Scan(&s.PointingAddress, &s.GcloudProject, &s.GcloudAuth)
	}

	return s, nil
}

func GetAccount(email string) (s Accounts, e error) {

	if len(email) == 0 {
		return s, errors.New(fmt.Sprintf("email can't empty"))
	}

	query := fmt.Sprintf("SELECT id, email, privateKey, domainPrivateKey, resource, apiKey, development FROM accounts WHERE email=? LIMIT 1")
	rows, e := db.Query(query, email)
	if e != nil {
		return s, e
	}

	for rows.Next() {
		var key, domainPrivateKey, resource, apiKey sql.NullString
		e = rows.Scan(&s.Id, &s.Email, &key, &domainPrivateKey, &resource, &apiKey, &s.Development)
		if e == nil {
			if key.Valid {
				s.PrivateKey = key.String
			}

			if resource.Valid {
				s.Resource = resource.String
			}

			if apiKey.Valid {
				s.ApiKey = apiKey.String
			}

			if domainPrivateKey.Valid {
				s.DomainPrivateKey = domainPrivateKey.String
			}
		}
	}

	if s.Id == 0 {
		return s, errors.New(fmt.Sprintf("email %s not found in database", email))
	}

	return s, e
}

func UpdateAccounts(email, privateKey, domainPrivateKey, resource interface{}) error {

	if privateKey != nil {
		stmt, err := db.Prepare("UPDATE accounts set privateKey=? WHERE email=?")
		if err != nil {
			return err
		}

		_, err = stmt.Exec(privateKey, email)
		stmt.Close()
		if err != nil {
			return err
		}
	}

	if domainPrivateKey != nil {
		stmt, err := db.Prepare("UPDATE accounts set domainPrivateKey=? WHERE email=?")
		if err != nil {
			return err
		}

		_, err = stmt.Exec(domainPrivateKey, email)
		stmt.Close()
		if err != nil {
			return err
		}
	}

	if resource != nil {
		stmt, err := db.Prepare("UPDATE accounts set resource=? WHERE email=?")
		if err != nil {
			return err
		}

		_, err = stmt.Exec(resource, email)
		stmt.Close()
		if err != nil {
			return err
		}
	}

	return nil
}

func InsertToken(token, value string) error {
	now := time.Now().Unix()

	stmt, err := db.Prepare("INSERT INTO validation (token, value, lastupdate) VALUES(?, ?, ?) ON DUPLICATE KEY UPDATE value=?, lastupdate=?")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(token, value, now, value, now)
	stmt.Close()
	if err != nil {
		return err
	}

	return nil
}

func RemoveToken(token string) error {

	stmt, err := db.Prepare("DELETE FROM validation WHERE token=?")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(token)
	stmt.Close()
	if err != nil {
		return err
	}

	return nil
}

func RemoveDomain(domain string) error {

	stmt, err := db.Prepare("DELETE FROM domains WHERE domain=?")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(domain)
	stmt.Close()
	if err != nil {
		return err
	}

	return nil
}

func ResetDomain(domain string) error {

	stmt, err := db.Prepare("UPDATE domains set resources = ? WHERE domain=?")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(sql.NullString{}, domain)
	stmt.Close()
	if err != nil {
		return err
	}

	return nil
}

func InsertDomain(domain, email, resources, serverid, privateKey interface{}, globalPrivateKey, wildcard bool) error {
	now := time.Now().Unix()
	stmt, err := db.Prepare("INSERT INTO domains (domain, email, privateKey, resources, serverid, lastupdate, globalPrivateKey, wildcard) VALUES(?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE resources=?, serverid=?, lastupdate=?")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(domain, email, privateKey, resources, serverid, now, globalPrivateKey, wildcard, resources, serverid, now)
	stmt.Close()
	if err != nil {
		return err
	}

	return nil
}

func GetValidation(token string) (s Validation, e error) {

	if len(token) == 0 {
		return s, errors.New(fmt.Sprintf("token can't empty"))
	}

	query := fmt.Sprintf("SELECT token, value, lastupdate FROM validation WHERE token=? LIMIT 1")
	rows, e := db.Query(query, token)
	if e != nil {
		return s, e
	}

	for rows.Next() {
		rows.Scan(&s.Token, &s.Value, &s.Lastupdate)
	}

	if len(s.Token) == 0 {
		return s, errors.New(fmt.Sprintf("token %s not found in database", token))
	}

	return s, e
}

type LastError struct {
	Time    int64  `json:"time"`
	Message string `json:"message"`
}

func SetDomainLastUpdate(domain string, em interface{}) (e error) {
	now := time.Now().Unix()

	es, _ := json.Marshal(LastError{
		Time:    now,
		Message: fmt.Sprintf("%s", em),
	})

	stmt, err := db.Prepare("UPDATE domains set lastupdate=?, lastError=? WHERE domain=?")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(now, string(es), domain)
	stmt.Close()
	if err != nil {
		return err
	}
	return e
}

func SetDomainResources(domain, resources string) (e error) {
	stmt, err := db.Prepare("UPDATE domains set resources=? WHERE domain=?")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(resources, domain)
	stmt.Close()
	if err != nil {
		return err
	}
	return e
}

func SetCertificates(domain, certificate, hash string, expired int64) (e error) {

	stmt, err := db.Prepare("UPDATE domains set certs=?, resources = ?, hash = ?, expired = ? WHERE domain=?")
	if err != nil {
		return err
	}
	_, err = stmt.Exec(certificate, sql.NullString{}, hash, expired, domain)
	stmt.Close()
	if err != nil {
		return err
	}
	return e
}

func GetDomainExpired(serverid interface{}) (s []Domain, e error) {
	var rows *sql.Rows
	query := fmt.Sprintf("SELECT domain, email, globalPrivateKey, wildcard, expired FROM domains WHERE serverid = ? AND autorenew = ? AND `resources` IS NULL  ORDER BY lastupdate DESC")
	rows, e = db.Query(query, serverid, 1)
	if e != nil {
		return s, e
	}

	for rows.Next() {
		var st Domain
		e = rows.Scan(&st.Domain, &st.Email, &st.GlobalPrivateKey, &st.Wildcard, &st.Expired)
		if e == nil && st.Expired != 0 {
			a := time.Unix(st.Expired, 0)
			delta := time.Now().Sub(a)
			var del float64 = delta.Hours() / 24
			if del > -30 {
				s = append(s, st)
			}

		}
	}

	return s, e

}

func GetallDomain() (s []Domain, e error) {
	var rows *sql.Rows
	query := fmt.Sprintf("SELECT `domains`.domain, `domains`.privateKey, `accounts`.domainPrivateKey, `domains`.certs, `domains`.globalPrivateKey, `domains`.hash FROM domains JOIN accounts ON `domains`.email = `accounts`.email WHERE `certs` IS NOT NULL")
	rows, e = db.Query(query)
	if e != nil {
		return s, e
	}

	for rows.Next() {
		var st Domain
		var privateKey, accprivateKey, hash sql.NullString
		e = rows.Scan(&st.Domain, &privateKey, &accprivateKey, &st.Certs, &st.GlobalPrivateKey, &hash)
		if e == nil {
			if st.GlobalPrivateKey == true {
				if accprivateKey.Valid == true {
					st.PrivateKey = accprivateKey.String
				}
			} else if st.GlobalPrivateKey == false {
				if privateKey.Valid == true {
					st.PrivateKey = privateKey.String
				}
			}

			s = append(s, st)

		}
	}

	return s, e

}

func GetDomainResources(domain, serverid interface{}) (s Domain, e error) {

	var rows *sql.Rows

	if domain != nil {
		query := fmt.Sprintf("SELECT domain, email, privateKey,  resources, lastupdate, globalPrivateKey FROM domains WHERE domain=? LIMIT 1")
		rows, e = db.Query(query, domain)
		if e != nil {
			return s, e
		}
	}

	if serverid != nil {
		query := fmt.Sprintf("SELECT domain, email, privateKey, resources, lastupdate, globalPrivateKey FROM domains WHERE serverid = ? AND `resources` IS NOT NULL AND resources != '' ORDER BY lastupdate ASC LIMIT 1")
		rows, e = db.Query(query, serverid)
		if e != nil {
			return s, e
		}
	}

	for rows.Next() {
		var privateKey, resource sql.NullString
		e = rows.Scan(&s.Domain, &s.Email, &privateKey, &resource, &s.Lastupdate, &s.GlobalPrivateKey)
		if e == nil {
			if resource.Valid {
				s.Resources = resource.String
			}

			if privateKey.Valid {
				s.PrivateKey = privateKey.String
			}
		}
	}

	return s, e
}
