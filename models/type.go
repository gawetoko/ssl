package models

type Accounts struct {
	Id               int64
	Email            string
	PrivateKey       string
	DomainPrivateKey string
	Resource         string
	ApiKey           string
	Development      bool
}

type Validation struct {
	Token      string
	Value      string
	Lastupdate int64
}

type Domain struct {
	Domain           string
	Email            string
	PrivateKey       string
	Certs            string
	Resources        string
	Serverid         string
	AutoRenew        bool
	Lastupdate       int64
	GlobalPrivateKey bool
	Wildcard         bool
	Hash             string
	Expired          int64
}

type Config struct {
	PointingAddress string
	GcloudProject   string
	GcloudAuth      string
}
