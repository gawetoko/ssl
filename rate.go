package main

import (
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"
)

var _ = fmt.Sprintf

var request = struct {
	sync.RWMutex
	req int64
}{
	req: 0,
}

var ips = make(map[string]int)
var abuse = make(map[string]int64)
var mutex = &sync.Mutex{}

var intervalRate *time.Ticker

func init() {

	intervalRate = time.NewTicker(time.Duration(1) * time.Second)
	go func() {
		for _ = range intervalRate.C {

			//timeFormat := "2006/01/02 15:04:05"
			//fmt.Printf("\r%s Request Per second %d \r", time.Now().Format(timeFormat), request.req)

			//reset request num per second
			request.Lock()
			request.req = 0
			request.Unlock()

			//mutex.Lock()
			//fmt.Println("ips:", ips, "abuse:", abuse)
			//mutex.Unlock()

			mutex.Lock()
			now := time.Now().Unix()
			for k, ss := range abuse {
				sl := now - ss
				if sl > 3600 {
					delete(abuse, k)
				}
			}

			for k, s := range ips {
				if s > 300 {
					abuse[k] = time.Now().Unix()
				}
				delete(ips, k)
			}
			mutex.Unlock()
		}
	}()

}

func getip(r *http.Request) (ip string) {
	ss := strings.Split(r.Header.Get("X-Forwarded-For"), ",")
	if len(ss) != 0 {
		return ss[0]
	}
	return ip
}

func ratelimit(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		request.Lock()
		request.req++
		request.Unlock()
		ip := getip(r)

		mutex.Lock()
		var l = ips[ip]
		if ips[ip] == 0 {
			ips[ip] = 1
		} else {
			ips[ip] = l + 1
		}
		mutex.Unlock()

		h.ServeHTTP(w, r)
		return

	})
}
