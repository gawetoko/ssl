package main

import (
	"bitbucket.org/gawetoko/ssl/config"
	"bitbucket.org/gawetoko/ssl/controllers"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Server struct {
}

func (this *Server) Run() {

	conf := config.Get()
	c := conf.Server

	var r *mux.Router = mux.NewRouter().StrictSlash(true)

	r.Handle("/api/create", &controllers.CreateSSL{}).Methods("POST")
	r.Handle("/.well-known/acme-challenge/check", &controllers.Check{}).Methods("GET")

	r.Handle("/.well-known/acme-challenge/{key}", &controllers.Validation{}).Methods("GET")
	r.Handle("/.well-known/acme-challenge/{key}", &controllers.Validation{}).Methods("PUT")
	r.Handle("/.well-known/acme-challenge/{key}", &controllers.Validation{}).Methods("POST")

	r.Handle("/", &controllers.Index{}).Methods("GET")

	servp := &http.Server{
		Addr:           fmt.Sprintf("%s:%d", c.Address, c.Port),
		Handler:        &httpHandle{handle: ratelimit(recoverWrap(r))},
		ReadTimeout:    3 * time.Second,
		WriteTimeout:   3 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	servp.SetKeepAlivesEnabled(false)

	log.Fatal(servp.ListenAndServe())

}

type httpHandle struct {
	handle http.Handler
}

func (this *httpHandle) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Server", "Gawetoko platfrom v1")
	this.handle.ServeHTTP(w, r)
	return
}

func recoverWrap(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		defer func() {
			if re := recover(); re != nil {
				w.Header().Set("Content-Type", "text/html")
				w.WriteHeader(500)
				var showlog bool = false
				if s, err := strconv.ParseBool(r.URL.Query().Get("debug")); err == nil {
					showlog = s
				}

				if showlog {
					w.Write([]byte(fmt.Sprintf("%s", re)))
				} else {
					w.Write([]byte("Something when wrong, try again later."))
				}

				return
			}

		}()

		ip := getip(r)
		mutex.Lock()
		abs := abuse[ip]
		mutex.Unlock()
		if abs != 0 {

			time.Sleep(30 * time.Second)
			w.Write([]byte(fmt.Sprintf("Http rate to much for this ip %s, slow down.", ip)))
			return

		} else {

			sd := handlers.CompressHandler(h)
			sd.ServeHTTP(w, r)
			return
		}

		return

	})
}
